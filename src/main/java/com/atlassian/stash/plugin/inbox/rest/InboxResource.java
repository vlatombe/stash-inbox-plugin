package com.atlassian.stash.plugin.inbox.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.pull.PullRequestRole;
import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.pull.PullRequestState;
import com.atlassian.stash.rest.data.RestPage;
import com.atlassian.stash.rest.data.RestPullRequest;
import com.atlassian.stash.rest.util.BadRequestException;
import com.atlassian.stash.rest.util.ResponseFactory;
import com.atlassian.stash.rest.util.RestResource;
import com.atlassian.stash.rest.util.RestUtils;
import com.atlassian.stash.user.PermissionValidationService;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.atlassian.stash.util.Page;
import com.atlassian.stash.util.PageRequest;
import com.atlassian.stash.util.PageRequestImpl;
import com.google.common.collect.ImmutableMap;
import com.sun.jersey.spi.resource.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Map;

@Path("/")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({RestUtils.APPLICATION_JSON_UTF8})
@Singleton
@AnonymousAllowed
public class InboxResource extends RestResource {

    private static final Logger log = LoggerFactory.getLogger(InboxResource.class);

    private final PullRequestService pullRequestService;
    private final StashAuthenticationContext authenticationContext;
    private final PermissionValidationService permissionValidationService;
    private final NavBuilder navBuilder;

    public InboxResource(PullRequestService pullRequestService, StashAuthenticationContext authenticationContext,
                         PermissionValidationService permissionValidationService, NavBuilder navBuilder, I18nService i18nService) {
        super(i18nService);
        this.pullRequestService = pullRequestService;
        this.authenticationContext = authenticationContext;
        this.permissionValidationService = permissionValidationService;
        this.navBuilder = navBuilder;
    }

    @SuppressWarnings("ConstantConditions")
    @GET
    @Path("pull-requests")
    public Response getPullRequests(@QueryParam("start") @DefaultValue(RestUtils.DEFAULT_START) int start,
                                    @QueryParam("limit") @DefaultValue(RestUtils.DEFAULT_LIMIT) int limit,
                                    @QueryParam("role") @DefaultValue("reviewer") String role) {
        permissionValidationService.validateAuthenticated();
        log.debug("Retrieving pull requests for user {}", authenticationContext.getCurrentUser().getDisplayName());

        PageRequest pageRequest = new PageRequestImpl(start, limit);

        Page<PullRequest> pullRequestPage = pullRequestService.findByParticipant(authenticationContext.getCurrentUser(),
                parseRole(role), false, PullRequestState.OPEN, null, pageRequest);

        return ResponseFactory.ok(
                new RestPage<RestPullRequest>(pullRequestPage, RestPullRequest.REST_TRANSFORM)
        ).build();
    }

    @SuppressWarnings("ConstantConditions")
    @GET
    @Path("pull-requests/count")
    public Response getPullRequestCount() {
        permissionValidationService.validateAuthenticated();
        log.debug("Retrieving pull request count for user {}", authenticationContext.getCurrentUser().getDisplayName());

        long count = pullRequestService.countForParticipant(authenticationContext.getCurrentUser(),
                PullRequestRole.REVIEWER, false, PullRequestState.OPEN);
        Map<String, Long> response = ImmutableMap.of("count", count);

        return ResponseFactory.ok(response).build();
    }

    private PullRequestRole parseRole(String role) {
        try {
            return PullRequestRole.valueOf(role.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new BadRequestException(
                    i18nService.getText("stash.rest.inbox.role.invalid", null, role, Arrays.asList(PullRequestRole.values()))
            );
        }
    }
}
