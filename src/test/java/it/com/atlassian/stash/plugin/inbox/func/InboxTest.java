package it.com.atlassian.stash.plugin.inbox.func;

import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.pageobjects.page.LoginPage;
import com.atlassian.stash.test.PullRequestTestHelper;
import com.atlassian.stash.test.TestContext;
import com.atlassian.stash.user.Permission;
import com.atlassian.webdriver.stash.StashTestedProduct;
import com.atlassian.webdriver.stash.element.PullRequestList;
import com.atlassian.webdriver.stash.page.ProjectListPage;
import com.atlassian.webdriver.stash.page.PullRequestOverviewPage;
import com.atlassian.webdriver.stash.page.PullRequestPage;
import com.atlassian.webdriver.testing.rule.SessionCleanupRule;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import com.atlassian.webdriver.testing.rule.WindowSizeRule;
import it.com.atlassian.stash.plugin.inbox.func.pageobjects.FeatureDiscoveryHelper;
import it.com.atlassian.stash.plugin.inbox.func.pageobjects.InboxDialog;
import it.com.atlassian.stash.plugin.inbox.func.pageobjects.InboxTrigger;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.*;
import org.springframework.core.io.ClassPathResource;

import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;


public class InboxTest {

    private static final String INBOX_PROJ = "PR_TEST";
    private static final String INBOX_REPO = "inbox_repo";
    private static final String GIT_PR_ZIP = "git/pull-requests.zip";
    private static final String SOURCE_BRANCH = "branch_that_has_same_file_modified_and_moved_src";
    private static final String TARGET_BRANCH = "branch_that_has_same_file_modified_and_moved_trgt";
    private static final String CREATOR = "pr_creator";
    private static final String REVIEWER = "pr_reviewer";
    private static final String PR_TITLE = "Some pull request";
    private static final StashTestedProduct STASH = TestedProductFactory.create(StashTestedProduct.class);

    protected PullRequestTestHelper pullRequest;

    @ClassRule
    public static TestContext classTestContext = new TestContext();

    @Rule
    public WindowSizeRule windowSizeRule = new WindowSizeRule();

    @Rule
    public WebDriverScreenshotRule screenshotRule = new WebDriverScreenshotRule();

    @Rule
    public TestContext testContext = new TestContext();

    @Rule
    public SessionCleanupRule cleanupRule = new SessionCleanupRule();

    @BeforeClass
    public static void setup() throws Exception {
        classTestContext
                .project(INBOX_PROJ)
                .repository(INBOX_PROJ, INBOX_REPO, new ClassPathResource(GIT_PR_ZIP))
                .user(CREATOR)
                .user(REVIEWER)
                .repositoryPermission(INBOX_PROJ, INBOX_REPO, CREATOR, Permission.REPO_WRITE)
                .repositoryPermission(INBOX_PROJ, INBOX_REPO, REVIEWER, Permission.REPO_READ);
    }

    @Before
    public void setupPullRequest() {
        pullRequest = testContext.pullRequest(new PullRequestTestHelper.Builder(CREATOR, CREATOR, PR_TITLE, "Description",
                INBOX_PROJ, INBOX_REPO, SOURCE_BRANCH, INBOX_PROJ, INBOX_REPO, TARGET_BRANCH).reviewers(REVIEWER));
    }

    @Test
    public void testStashInboxAsReviewer() {
        STASH.visit(LoginPage.class).login(REVIEWER, REVIEWER, ProjectListPage.class);
        PullRequestPage page = STASH.visit(PullRequestOverviewPage.class, INBOX_PROJ, INBOX_REPO, pullRequest.getId());

        FeatureDiscoveryHelper featureDiscoveryDialog = STASH.getPageBinder().bind(FeatureDiscoveryHelper.class);
        featureDiscoveryDialog.dismissAll();

        InboxTrigger inboxTrigger = STASH.getPageBinder().bind(InboxTrigger.class);
        waitUntilFalse(inboxTrigger.inboxIsEmpty());
        waitUntilTrue(inboxTrigger.isCountVisible());
        assertEquals("1", inboxTrigger.getCountText());

        InboxDialog inbox = inboxTrigger.open();
        assertTrue(inbox.isShowingReviewing());
        assertFalse(inbox.isActivePullRequestListEmpty());
        List<PullRequestList.Entry> pullRequestList = inbox.getActivePullRequestList().getRows();
        assertThat(pullRequestList, hasSize(1));
        assertThat(pullRequestList, hasItem(entry(PR_TITLE)));

        inbox.switchToCreated();
        assertTrue(inbox.isShowingCreated());
        assertTrue(inbox.isActivePullRequestListEmpty());

        // The inbox obscures the Approve button so needs to be closed
        inbox.close();

        // Approving/unapproving the pull request should update inbox count
        page.getApproveButton().clickButton();
        waitUntilTrue(inboxTrigger.inboxIsEmpty());

        page.getApproveButton().clickButton();
        waitUntilFalse(inboxTrigger.inboxIsEmpty());
    }

    @Test
    public void testStashInboxAsCreator() {
        STASH.visit(LoginPage.class).login(CREATOR, CREATOR, ProjectListPage.class);
        InboxTrigger inboxTrigger = STASH.getPageBinder().bind(InboxTrigger.class);
        waitUntilTrue(inboxTrigger.inboxIsEmpty());

        InboxDialog inbox = inboxTrigger.open();
        assertTrue(inbox.isShowingReviewing());
        assertTrue(inbox.isActivePullRequestListEmpty());

        inbox.switchToCreated();
        List<PullRequestList.Entry> pullRequestList = inbox.getActivePullRequestList().getRows();
        assertThat(pullRequestList, hasSize(1));
        assertThat(pullRequestList, hasItem(entry(PR_TITLE)));
    }

    private Matcher<PullRequestList.Entry> entry(final String title) {
        return new TypeSafeMatcher<PullRequestList.Entry>() {
            @Override
            protected boolean matchesSafely(PullRequestList.Entry entry) {
                return title.equals(entry.getTitle());
            }
            @Override
            public void describeTo(Description description) {
                description.appendText("a list entry with the title ").appendValue(title);
            }
        };
    }
}
